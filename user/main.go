package main

import (
	beego "github.com/beego/beego/v2/server/web"
	_ "user/models"
	_ "user/routers"
)

func main() {
	beego.Run()
}
