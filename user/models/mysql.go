package models

import (
	"errors"
	"fmt"
	"github.com/beego/beego/v2/client/orm"
	_ "github.com/go-sql-driver/mysql"
)

type User struct {
	Id       int64
	Mobile   string `orm:"unique"` //唯一
	Password string
}

func init() {
	orm.RegisterModel(new(User))
	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", "root:root@tcp(127.0.0.1:3306)/1910a")
	orm.RunSyncdb("default", false, true)
	orm.Debug = true
}

func Register(u User) (int64, error) {
	o := orm.NewOrm()
	id, err := o.Insert(&u)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func Logins(mobile string) (User, error) {
	o := orm.NewOrm()
	l := User{Mobile: mobile}
	_, err := o.QueryTable("User").Filter("mobile", mobile).All(&l)
	fmt.Println(err)
	if err != nil {
		return User{}, errors.New("账号错误")
	}
	fmt.Println(l)
	return l, err

}
