package routers

import (
	beego "github.com/beego/beego/v2/server/web"
	"user/controllers"
)

func init() {
	beego.Router("/register", &controllers.RegisterController{}, "post:Register")
	beego.Router("/login", &controllers.LoginController{}, "post:Login")
}
